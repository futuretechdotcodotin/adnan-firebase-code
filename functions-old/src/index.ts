
import * as functions from 'firebase-functions';

// A simple callable function for a sanity check
export const testFunction = functions.https.onCall( async (data, context) => {
    const uid  = context.auth && context.auth.uid;
    const message = data.message;

    return `${uid} sent a message of ${message}`
});


export {
    stripeCreateCharge,
    stripeGetCharges
} from './charges';
