"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
// A simple callable function for a sanity check
exports.testFunction = functions.https.onCall(async (data, context) => {
    const uid = context.auth && context.auth.uid;
    const message = data.message;
    return `${uid} sent a message of ${message}`;
});
var charges_1 = require("./charges");
exports.stripeCreateCharge = charges_1.stripeCreateCharge;
exports.stripeGetCharges = charges_1.stripeGetCharges;
//# sourceMappingURL=index.js.map