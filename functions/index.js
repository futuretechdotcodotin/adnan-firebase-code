const functions = require('firebase-functions');
const app = require("express")();
const stripe = require("stripe")('sk_test_p5dc9s7iZ3mYBJoPhhST3Cr600Bfsgs6T6');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function (req, res, next) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});
app.post("/", async function (req, res) {
    const token = req.body.stripeToken; // Using Express
    const amount = req.body.amount;
    if (token) {
            stripe.customers.create({
                email: req.body.email, // customer email
                name: req.body.name,
                description: "COVID-19 Lebanese Expat Fund",
                source: token // token for the card
            }).then(customer =>
                stripe.charges.create({ // charge the customer
                    amount,
                    description: "COVID-19 Lebanese Expat Fund",
                    currency: "usd",
                    customer: customer.id
                }))
                .then(charge => res.redirect("https://donate.presentail.com/thank-you.html")).catch(err => res.send(err));
    } else {
        res.status(400).send();
    }
});

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.api = functions.https.onRequest(app);
